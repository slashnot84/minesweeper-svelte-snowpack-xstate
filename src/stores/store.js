
import { writable } from 'svelte/store'

const { set, update, subscribe } = writable("RAM KUMAR")
export default {
    subscribe,
    set,
    update,
    changeName: (name) => {
        update(state=>{
            return state = name
        })
    }
}