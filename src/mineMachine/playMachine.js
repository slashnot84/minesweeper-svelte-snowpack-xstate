import { playActions, playGaurds } from './playOptions';

const { setNoOfMines } = playActions
const { isMineFound } = playGaurds

/* -- State Definition
---------------------------------------------------- */
const states = {
    Playing: {
        on: {
            BLOCK_SELECT: [
                {
                    target: 'FinishPlaying',
                    cond: isMineFound
                },
                {
                    target: 'ContinuePlaying'
                }
            ]
        }
    },
    ContinuePlaying: {
        on: {
            '': {
                target: 'Playing',
                actions: [setNoOfMines]
            }
        }
    },
    FinishPlaying: {
        type: 'final'
    }
}

/* -- State Machine Definition
---------------------------------------------------- */
const playMachine = {
    id: 'playMachine',
    initial: 'Playing',
    states
}

export default playMachine;