// @ts-nocheck
import { Machine } from "xstate";
import mineMachine from "./mineMachine";

const AppMachine = Machine(mineMachine);

export default AppMachine;

