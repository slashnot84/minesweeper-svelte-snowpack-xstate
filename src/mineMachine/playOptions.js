
/* -- Play Machine Actions
---------------------------------------------------- */
const playActions = {
    setNoOfMines(ctx, event) {
        const { yId, xId } = event.payload

        const adj = playActions.getAdjacentElements(yId, xId, ctx);
        ctx.matrix[yId][xId] = playActions.getNoOfMines(ctx.matrix, adj);
    },

    getAdjacentElements(x, y, ctx) {
        return [
            [x + 1, y],
            [x - 1, y],
            [x, y + 1],
            [x, y - 1],
            [x - 1, y - 1],
            [x + 1, y + 1],
            [x - 1, y + 1],
            [x + 1, y - 1],
        ];
    },

    getNoOfMines(matrix = [], arrList = []) {
        const hasMine = arrList.filter((arrItem) => {
            return (
                matrix[arrItem[0]] && [arrItem[1]] &&
                matrix[arrItem[0]][arrItem[1]] === true
            );
        });
        return hasMine.length;
    },
}

/* -- Play Machine Guards
---------------------------------------------------- */
const playGaurds = {
    isMineFound(ctx, event) {
        const { yId, xId } = event.payload
        return ctx.matrix[yId][xId] === true
    }
}

export { playActions, playGaurds }