import mineActions from "./mineActions";
import playMachine from "./playMachine";

const { generateRandomMatrix, resetContext } = mineActions;
const context = {
    matrix: [],
}

/* -- State Definition
---------------------------------------------------- */
const states = {
    SelectGame: {
        on: {
            START_GAME: 'InitGame'
        }
    },
    InitGame: {
        on: {
            '': {
                target: 'GamePlay',
                actions: [generateRandomMatrix]
            }
        }
    },
    GamePlay: {
        ...playMachine,
        on: {
            RESET: 'InitGame',
        },
        onDone: 'GameWon'
    },
    GameWon: {
        on: {
            RESTART: {
                target: 'SelectGame',
                actions: [resetContext]
            }
        }
    }
}

/* -- State Machine Definition
---------------------------------------------------- */
const mineMachine = {
    id: 'mineMachine',
    initial: 'SelectGame',
    context,
    states
}

export default mineMachine;