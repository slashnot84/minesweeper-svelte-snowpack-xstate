let noOfMines = 6;
let matrix = [];

/* -- Mine Machine Actions
---------------------------------------------------- */
const mineActions = {
    generateRandomMatrix(ctx) {
        mineActions.createMatrix()
        mineActions.createMines(noOfMines)
        ctx.matrix = matrix
    },

    resetContext(ctx) {
        console.log("resetContext")
        matrix = []
        ctx.matrix = []
    },

    createMatrix() {
        console.log("CREATING MATRIX....");
        matrix = new Array(noOfMines)
            .fill(false)
            .map(() => new Array(noOfMines).fill(false));
    },

    createMines(mines) {
        console.log("CREATING MINES....");
        while (mines > 0) {
            let x = this.getRandomInt(noOfMines);
            let y = this.getRandomInt(noOfMines);

            if (!matrix[y][x]) {
                matrix[y][x] = true;
                mines--;
            }
        }
    },

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
}

export { mineActions }
export default mineActions