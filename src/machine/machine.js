import { assign, send } from 'xstate'
let noOfMines = 6;
let restart;
let selected = [];
let attemptsCount = 1;
let gameOver = false;
let cheatCode = [];
let unlocked = false;

let matrix = [];

const createMatrix = () => {
    matrix = new Array(noOfMines)
        .fill(false)
        .map(() => new Array(noOfMines).fill(false));
};

const createMines = (mines) => {
    while (mines > 0) {
        let x = getRandomInt(noOfMines);
        let y = getRandomInt(noOfMines);

        if (!matrix[y][x]) {
            matrix[y][x] = true;
            mines--;
        }
    }
};

const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
};

const getAdjacentElements = (x, y, ctx) => {
    return [
        [x + 1, y],
        [x - 1, y],
        [x, y + 1],
        [x, y - 1],
        [x - 1, y - 1],
        [x + 1, y + 1],
        [x - 1, y + 1],
        [x + 1, y - 1],
    ];
};

const getNoOfMines = (matrix = [], arrList = []) => {
    const hasMine = arrList.filter((arrItem) => {
        return (
            matrix[arrItem[0]] && [arrItem[1]] &&
            matrix[arrItem[0]][arrItem[1]] === true
        );
    });
    return hasMine.length;
};


const gameState = {
    initial: 'Start',
    states: {
        Start: {
            on: {
                DISPLAY_DATA: [
                    {
                        target: 'GameOver',
                        cond: (ctx, event) => {
                            const { yId, xId } = event.payload
                            return ctx.matrix[yId][xId] === true
                        }
                    },
                    {
                        target: 'GameContinue'
                    }
                ]
            }
        },
        GameContinue: {
            on: {
                '': {
                    target: 'Start',
                    actions: (ctx, event) => {
                        const { yId, xId } = event.payload

                        const adj = getAdjacentElements(yId, xId, ctx);
                        ctx.matrix[yId][xId] = getNoOfMines(ctx.matrix, adj);
                    }
                }
            }
        },
        GameOver: {
            type: 'final'
        }
    },
    // onDone: "ParentGameDone"
}


// Test Machine
const machine = {
    key: "testState",
    initial: "Idle",
    context: {
        adjacents: 0,
        isGameOver: false,
        counter: 0,
        matrix,
        selected
    },
    states: {
        Idle: {
            invoke: {
                id: 'incInterval',
                src: (context, ev) => (callback) => {
                    createMatrix();
                    createMines(noOfMines);
                    context.matrix = matrix;
                    callback('BLOCK_SELECTED')
                },
            },
            on: {
                BLOCK_SELECTED: "DisplayResult"
            }
        },
        DisplayResult: {
            ...gameState,
            onDone: "ParentGameDone"
        },
        ParentGameDone: {
            on:{
                RESET: 'Idle'
            }
        }
    }
}

export default machine;